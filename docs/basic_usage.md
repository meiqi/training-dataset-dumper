This section describes how to run the basic AnalysisBase dataset dumper. This is the recommend workflow in almost all cases.

If you need to do something that depends on the ATLAS geometry service, or the ATLAS magnetic field (i.e. track extrapolation, running full b-tagging, or jet reconstruction), see the Advanced section.

### Running locally

After having followed the installation instructions and set up the training dataset dumper, you can invoke the executable with

```bash
dump-single-btag -c <path to configuration file> <paths to xAOD(s)>
```

The configuration files are located in [`configs/single-btag`]({{repo_url}}-/tree/r22/configs).
Alongside the `output.h5` output file, the `userJobMetadata.json` contains information about the run, 
including inference timing for taggers.

### Running on the grid

In addition to the setup of the training dataset dumper described in the installation instructions, you need to go to the directory where you checked out the package and run

```bash
source training-dataset-dumper/BTagTrainingPreprocessing/grid/setup.sh
```

You are now set up to submit jobs on the grid using the `grid-submit` script.

If you are unfamiliar with the `grid-submit` script, be sure to use the `-h` flag to get usage information.
Note that, as mentioned in the usage, the mode positional argument (e.g. `single-btag`) must come after any optional flags.
The available optional flags and modes are listed when using `-h`, and can also be found in the script's [source code]({{repo_url}}-/blob/r22/BTagTrainingPreprocessing/grid/grid-submit#L16).

When submitting jobs, it's good practice to initially use the `-d` flag to perform a dry run:

```bash
grid-submit -d single-btag
```

This will run the script without actually submitting any jobs to the grid, allowing you to double check the config and inputs before submission.
The script will error if you have uncommitted changes to the repository in order to ensure reproducibility.

You can then run the grid submission, tagging the current code state, using this command:

```bash
grid-submit -t <tagname> single-btag
```

You can overwrite the mode's default configuration file using `-c <config.json>`.
Similarly, the executable can be specified with `-s <script>`, and the inputs file with `-i <inputs.txt>`.
The inputs file is a text file containing a list of input datasets to submit over, with one DSID per line.
Blank lines and lines starting with a `#` are ignored.
The default datasets for each mode can be found [here]({{repo_url}}-/tree/r22/BTagTrainingPreprocessing/grid/inputs).

If you are dumping samples that will be reused you should tag the current state of the repository by using the `-t <tagname>` flag. Note that:

- The date will be prepended to the tag name you provide. 
- The tag will be pushed to your personal fork. 
- The script will error if you attempt to push a tag to the main repository instead of a fork. [See here](development.md#contributing-guidelines) for more information about working from a fork.


#### Output dataset names

The `grid-submit` script will automatically create output dataset names (DSIDs) for your jobs. Given an input DSID, such as 

```
mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYSVAL.e6337_s3681_r13167_p4931
```

the script will strip out the simulation description and keep the sample id (here `410470`) and the [production tags](https://ftag.docs.cern.ch/software/data/) (`e6337_s3681_r13167_p4931`), and then append information about the dumping job.
First, `tdd` is appended to easily identify the dataset as coming from the training dataset dumper, followed by the config file basename.
Next added are the release version or buildstamp, followed by the submission job ID.
When using `-t`, the job ID is simply the tag name.
The output DSID might then be 

```
user.x.410470.e6337_s3681_r13167_p4931.tdd.EMPFlow.22_2_68.22-04-23-T173636.22-04-20_tagname
```
If not using `-t`, but submitting with a clean working dir (all changes commmitted), [`git describe`](https://git-scm.com/docs/git-describe) is used to provide a unique reference to the latest commit from a previous tag.
If forcing submission with uncommitted changes using `-f`, a timestamp is used as the job ID.