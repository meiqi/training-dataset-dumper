"""
Here we construct a retagging sequence that takes a few variables
Such as :
  the input track, jet collections
  list of tracking systematics to run over
  option to merge LRT and standard tracks
This allows the users to perform retagging with more flexibilities
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from BTagTrainingPreprocessing.trackUtil import LRTMerger, applyTrackSys

from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg
from BTagging.BTagRun3Config import RetagRenameInputContainerCfg, GetTaggerTrainingMap
from BTagging.BTagRun3Config import BTagAlgsCfg


def retagging(
    cfgFlags,
    merge,
    track_collection="InDetTrackParticles",
    jet_collection="AntiKt4EMPFlowJets",
    sys_list=[],
):

    acc = ComponentAccumulator()

    pvCol = "PrimaryVertices"
    jet_collection_nosuffix = jet_collection.replace("Jets", "")
    muon_collection = "Muons"
    track_collection = track_collection

    # get list of taggers to run
    trainingMap = GetTaggerTrainingMap(jet_collection_nosuffix)

    if sys_list:
        acc.merge(applyTrackSys(sys_list, track_collection, jet_collection))
        track_collection = "InDetTrackParticles_Sys"

    # The decoration on tracks needs to happen before the track merging step
    # Decorate the standard tracks first
    acc.merge(
        BTagTrackAugmenterAlgCfg(
            cfgFlags,
            TrackCollection=track_collection,
            PrimaryVertexCollectionName=pvCol,
        )
    )

    if merge:
        # Decorate the LRT tracks
        acc.merge(
            BTagTrackAugmenterAlgCfg(
                cfgFlags,
                TrackCollection="InDetLargeD0TrackParticles",
                PrimaryVertexCollectionName=pvCol,
            )
        )

        # merge the LRT and standard tracks into a single view collection
        acc.merge(LRTMerger())
        track_collection = "InDetWithLRTTrackParticles"

    # rename containers to avoid clashes
    acc.merge(RetagRenameInputContainerCfg("_retag", jet_collection_nosuffix))
    acc.merge(JetTagCalibCfg(cfgFlags))

    # add the main b-tagging config
    acc.merge(
        BTagAlgsCfg(
            cfgFlags,
            JetCollection=jet_collection,
            nnList=trainingMap,
            trackCollection=track_collection,
            muons=muon_collection,
            renameTrackJets = True,
            AddedJetSuffix = ''
    ))

    return acc
