#include "JetTruthSummaryDecorator.hh"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"


// the constructor just builds the decorator
JetTruthSummaryDecorator::JetTruthSummaryDecorator(const std::string& link_name, const std::string& out_name)
    : m_truth_particle_links(link_name),
      m_deco_n_truth("n_truth_" + out_name),
      m_deco_min_dr("min_dr_truth_" + out_name) {}

// return min deltaR(jet,particles), or infinity if no particles exist
float JetTruthSummaryDecorator::getMinimumDeltaR(
    const xAOD::Jet& jet,
    const PartLinks& part_links) const {
  
  std::vector<float> delta_r = {INFINITY};
  for (const PartLink& tl : part_links) {

    // cast to truth particle
    const auto* truth_particle = dynamic_cast<const xAOD::TruthParticle*>(*tl);
    if (!truth_particle) {
      throw std::runtime_error("Can't cast xAOD::IParticle to xAOD::TruthParticle");
    }
    
    // get minimum dR
    float dr = jet.p4().DeltaR(truth_particle->p4());
    delta_r.push_back(dr);
  }

  return *std::min_element(delta_r.begin(), delta_r.end());
}

// this call actually does the work on the jet
void JetTruthSummaryDecorator::decorate(
    const xAOD::Jet& jet) const {

  // get linked truth particles
  auto part_links = m_truth_particle_links(jet);

  // decorate
  m_deco_n_truth(jet) = part_links.size();
  m_deco_min_dr(jet) = getMinimumDeltaR(jet, part_links);
}
