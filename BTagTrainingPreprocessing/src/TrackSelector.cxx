#include "TrackSelector.hh"
#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"


TrackSelector::TrackSelector(TrackSelectorConfig config,
                             const std::string& link_name):
  m_track_associator(nullptr),
  m_track_select_cfg(config.cuts),
  m_acc(config.ip_prefix)
{
  if (config.btagging_link.size() == 0) {
    AE::ConstAccessor<PartLinks> acc(link_name);
    m_track_associator = [acc](const AE& jet) -> Tracks {
      Tracks tracks;
      for (const auto& link: acc(jet)) {
        if (!link.isValid()) {
          throw std::logic_error("invalid particle link in TrackSelector");
        }
        const auto* trk = dynamic_cast<const xAOD::TrackParticle*>(*link);
        if (!trk) {
          throw std::logic_error("iparticle does not cast to Track");
        }
        tracks.push_back(trk);
      }
      return tracks;
    };
  } else {
    AE::ConstAccessor<ElementLink<xAOD::BTaggingContainer>> btag_link(
      config.btagging_link);
    AE::ConstAccessor<TrackLinks> acc(link_name);
    m_track_associator = [acc, btag_link](const AE& jet) -> Tracks {
      Tracks tracks;
      const xAOD::BTagging* btag = *btag_link(jet);
      if (!btag) {
        throw std::logic_error("missing btaggingLink in TrackSelector");
      }
      for (const auto& link: acc(*btag)) {
        if (!link.isValid()) {
          throw std::logic_error("invalid track link in TrackSelector");
        }
        tracks.push_back(*link);
      }
      return tracks;
    };
  }
}


TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto& tp : m_track_associator(jet)) {
    if (passed_cuts(*tp)) {
      tracks.push_back(tp);
    }
  }
  return tracks;
}

bool TrackSelector::passed_cuts(const xAOD::TrackParticle& tp) const
{
  static SG::AuxElement::ConstAccessor<unsigned char> pix_hits("numberOfPixelHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_holes("numberOfPixelHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_shared("numberOfPixelSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_dead("numberOfPixelDeadSensors");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_hits("numberOfSCTHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_holes("numberOfSCTHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_shared("numberOfSCTSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_dead("numberOfSCTDeadSensors");

  if (std::abs(tp.eta()) > m_track_select_cfg.abs_eta_maximum)
          return false;
  double n_module_shared = (pix_shared(tp) + sct_shared(tp) / 2);
  if (n_module_shared > m_track_select_cfg.si_shared_maximum)
    return false;
  if (tp.pt() <= m_track_select_cfg.pt_minimum)
    return false;
  if (std::isfinite(m_track_select_cfg.d0_maximum) &&
      std::abs(m_acc.d0(tp)) >= m_track_select_cfg.d0_maximum)
    return false;
  if (std::isfinite(m_track_select_cfg.z0_maximum) &&
      std::abs(m_acc.z0SinTheta(tp)) >= m_track_select_cfg.z0_maximum)
    return false;
  if (pix_hits(tp) + pix_dead(tp) + sct_hits(tp) + sct_dead(tp) < m_track_select_cfg.si_hits_minimum)
    return false;
  if ((pix_holes(tp) + sct_holes(tp)) > m_track_select_cfg.si_holes_maximum)
    return false;
  if (pix_holes(tp) > m_track_select_cfg.pix_holes_maximum)
    return false;
  return true;
}


