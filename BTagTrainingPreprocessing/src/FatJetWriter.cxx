#include "FatJetWriter.hh"
#include "FatJetWriterConfig.hh"

#include "BTagJetWriterUtils.hh"
#include "SubstructureAccessors.hh"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"




FatJetWriter::FatJetWriter(
  H5::Group& output_file,
  const FatJetWriterConfig& config):
  m_ssa(new SubstructureAccessors)
{

  H5Utils::Compression c = H5Utils::Compression::STANDARD;
  // create the variable fillers
  JetConsumers fillers;
  if (config.jet_float_variables.size() > 0) {
    add_jet_float_fillers(fillers, config.jet_float_variables, NAN, c);
  }
  add_jet_int_variables(fillers, config.jet_int_variables);

  // some things like 4 momenta have to be hand coded
  FloatFiller pt = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->pt();
  };
  fillers.add("pt", pt);

  FloatFiller eta = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->eta();
  };
  fillers.add("eta", eta);

  if (config.write_kinematics_relative_to_parent) {
    add_parent_fillers(fillers);
  }
  if (config.parent_jet_int_variables.size() > 0) {
    add_parent_jet_int_variables(fillers, config.parent_jet_int_variables);
  }

  add_event_info(fillers, config.event_info, c);
  if (config.write_substructure_moments) add_substructure(fillers);

  // now create the writer
  m_hdf5_jet_writer = new JetOutputWriter(output_file, config.name, fillers);
}

FatJetWriter::~FatJetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
  delete m_hdf5_jet_writer;
  delete m_ssa;
}

FatJetWriter::FatJetWriter(FatJetWriter&& old):
  m_hdf5_jet_writer(old.m_hdf5_jet_writer),
  m_ssa(old.m_ssa)
{
  old.m_hdf5_jet_writer = nullptr;
  old.m_ssa = nullptr;
}

void FatJetWriter::write_with_parent(const xAOD::Jet& jet,
                                      const xAOD::Jet& parent,
                                      const xAOD::EventInfo* mc_event_info) {
  JetOutputs jo;
  jo.event_info = mc_event_info;
  jo.jet = &jet;
  jo.parent = &parent;
  m_hdf5_jet_writer->fill({jo});
}

void FatJetWriter::add_substructure(JetConsumers& vars) {
  FloatFiller mass = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->m();
  };
  vars.add("mass", mass);

  FloatFiller c2 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->c2(*jo.jet);
  };
  vars.add("C2", c2);

  FloatFiller d2 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->d2(*jo.jet);
  };
  vars.add("D2", d2);

  FloatFiller e3 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->e3(*jo.jet);
  };
  vars.add("e3", e3);

  FloatFiller tau21 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->tau21(*jo.jet);
  };
  vars.add("Tau21_wta", tau21);

  FloatFiller tau32 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->tau32(*jo.jet);
  };
  vars.add("Tau32_wta", tau32);

  FloatFiller fw20 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->fw20(*jo.jet);
  };
  vars.add("FoxWolfram20", fw20);

}

