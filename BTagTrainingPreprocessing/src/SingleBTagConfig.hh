#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include "DL2Config.hh"

#include "TrackSelectorConfig.hh"
#include "TruthSelectorConfig.hh"
#include "TrackSortOrder.hh"
#include "HitWriterConfig.hh"
#include "HitDecoratorConfig.hh"

#include "JetWriters/JetConstituentWriterConfig.h"
#include "JetWriters/JetLinkWriterConfig.h"

// no idea why the forward declare fails in analysis base :'(
#ifdef XAOD_STANDALONE
#include <nlohmann/json.hpp>
#else
#include <nlohmann/json_fwd.hpp>
#endif

#include <filesystem>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <optional>

typedef std::map<std::string,std::vector<std::string>> VariableList;

enum class JetCleanOption {none, event, jet};

struct TrackConfig {
  TrackSortOrder sort_order;
  TrackSelectorConfig selection;
  JetConstituentWriterConfig writer;
  std::string input_name;
};

struct TruthOutputConfig {
  std::string name;
  size_t n_to_save;
  TrackSortOrder sort_order;
};

struct TruthConfig {
  std::optional<TruthSelectorConfig> selection;
  std::vector<std::string> merge;
  float overlap_dr;
  std::string association_name;
  std::optional<TruthOutputConfig> output;
  std::optional<bool> decorate;
};

struct DecorateConfig {
  bool jet_aug;
  bool btag_jes;
  bool soft_muon;
  bool track_truth_info;
  bool track_sv_info;
  bool track_lepton_id;
  bool do_vrtrackjets_fix; // PG: temporary fix for missing VR track jet decorators
  bool do_heavyions;
  bool lepton_decay_label;
  bool truth_pileup;
};

struct JetCalibrationConfig {
  std::string collection;
  std::string configuration;
  std::string seq;
  std::string area;
};

struct SelectionConfig {
  bool truth_jet_matching;
  bool truth_primary_vertex_matching;
  size_t minimum_jet_constituents;
  float minimum_jet_pt;
  float maximum_jet_absolute_eta;
  float minimum_jvt;
  JetCleanOption jet_cleaning;
};

struct HitConfig {
  HitWriterConfig writer;
  HitDecoratorConfig decorator;
};

struct SubjetConfig {
  std::string input_name;
  std::string output_name;
  size_t n_subjets_to_save;
  size_t num_const;
  double min_jet_pt;
  double max_abs_eta;
  std::string btagging_link;
  std::map<std::string,std::vector<std::string>> variables;
};

struct SingleBTagConfig {
  std::string tool_prefix;
  std::string jet_collection;
  std::optional<JetCalibrationConfig> calibration;
  SelectionConfig selection;
  std::string vertex_collection;
  std::string btagging_link;
  std::string nntc;
  std::vector<DL2Config> dl2_configs;
  VariableList btag;
  std::map<std::string, std::string> default_flag_mapping;
  std::vector<TrackConfig> tracks;
  std::vector<TruthConfig> truths;
  std::optional<HitConfig> hits;
  std::vector<SubjetConfig> subjet_configs;
  DecorateConfig decorate;
  bool force_full_precision = false;
  std::optional<JetLinkWriterConfig> flow;
};

SingleBTagConfig get_singlebtag_config(const std::filesystem::path& cfg);
SingleBTagConfig get_singlebtag_config(const nlohmann::ordered_json& nlocfg,
                                       const std::string& tool_prefix);
void force_full_precision(SingleBTagConfig&);


#endif
