#!/usr/bin/env python3

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

from BTagTrainingPreprocessing import trigger as trig
from BTagTrainingPreprocessing import mctc
from BTagTrainingPreprocessing import dumper

import sys


def get_args():
    default_chain = 'HLT_j20_0eta290_020jvt_boffperf_pf_ftf_L1J15'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('-t','--threads', type=int, default=0)
    parser.add_argument('-n','--chain', default=default_chain, **dh)
    return parser.parse_args()

def run():
    args = get_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags

    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.Concurrency.NumThreads = args.threads
    if args.threads:
        cfgFlags.Concurrency.NumConcurrentEvents = args.threads

    cfgFlags.lock()

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = dumper.getMainConfig(cfgFlags, args)

    ca.merge(mctc.getMCTC())
    
    ca.merge(trig.pflowDumper(cfgFlags, chain=args.chain))
    ca.merge(dumper.getDumperConfig(args))

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
