#ifndef JET_ASSOCATION_WRITER_CONFIG_H
#define JET_ASSOCATION_WRITER_CONFIG_H

#include "JetConstituentWriterConfig.h"

struct JetLinkWriterConfig
{
  JetConstituentWriterConfig constituents;
  std::string accessor;
};

#endif
